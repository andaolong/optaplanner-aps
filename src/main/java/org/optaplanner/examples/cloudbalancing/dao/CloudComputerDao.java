package org.optaplanner.examples.cloudbalancing.dao;

import org.optaplanner.examples.cloudbalancing.domain.CloudComputer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author andaolong
 * @time 2021/3/25-9:23
 * 从数据库中获取computer的list
 */
@Component
public class CloudComputerDao {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    public List<CloudComputer> getCloudComputerListFromDB() {

        String sql = "select * from computer";

        /**
         * 通过spring的jdbc的BeanPropertyRowMapper从数据库读取数据到bean类的时候，
         * 一定要保证，bean类里面的成员变量的名称和数据库中的一致，不然从数据库中读取的数据也映射不到类的成员变量身上去
         * */
        /**
         * 如果，类中的某个变量名称和数据库中对应的字段名称确实是不想改成一样的(比如数据库是由甲方提供的，格式不能随便改)，
         * 或者是类型不一样，比如数据库中是String类型，而bean类中某一项成员变量是某种object类型，
         * 那么可以不用BeanPropertyRowMapper，而是用RowMapper，然后重写一下RowMapper中的mapRow方法
         * 参考链接：https://www.jianshu.com/p/be60a81e2fe7
         */
        return jdbcTemplate.query(sql, new BeanPropertyRowMapper<>(CloudComputer.class));
    }
}
