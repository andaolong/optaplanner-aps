package org.optaplanner.examples.apsplanning.service;

import org.optaplanner.examples.apsplanning.dao.ProcessDao;
import org.optaplanner.examples.apsplanning.domain.Process;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author andaolong
 * @time 2021/3/27-15:32
 * @describe
 */
@Service
public class ProcessService {

    @Autowired
    private ProcessDao processDao;

    public Process getProcessByProcessId(String processId) {
        return processDao.getProcessByProcessIdFromDB(processId);
    }

    public List<Process> getAllProcess(){
        return processDao.getAllProcess();
    }

}

