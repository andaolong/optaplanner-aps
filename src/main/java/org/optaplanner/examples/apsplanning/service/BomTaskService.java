package org.optaplanner.examples.apsplanning.service;

import org.optaplanner.examples.apsplanning.dao.BomTaskDao;
import org.optaplanner.examples.apsplanning.dao.ProcessDao;
import org.optaplanner.examples.apsplanning.domain.BomTask;
import org.optaplanner.examples.apsplanning.domain.Process;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author andaolong
 * @time 2021/3/27-15:18
 * @describe
 */
@Service
public class BomTaskService {

    @Autowired
    private BomTaskDao bomTaskDao;
    @Autowired
    private ProcessDao processDao;
    @Autowired
    private ProcessService processService;

    //获取到bomTask，并填入对应的process
    public List<BomTask> getBomTaskList() {

        List<BomTask> bomTaskList = bomTaskDao.getBomTaskListFromDB();

        for (BomTask bomTask : bomTaskList){
            //根据device的deviceId获取到device对应的machiningType的list

            //System.out.println("准备传进去的processId是："+bomTask.getProcessId());
            //System.out.println("看看能不能获取到bomId是："+bomTask.getBomId());

            Process process = processDao.getProcessByProcessIdFromDB(bomTask.getProcessId());

            //System.out.println("看看有没有获取到process："+process);
            //System.out.println("获取到process的machiningType："+process.getRequiredMachiningType());

            //将machiningType的list填入每个device
            bomTask.setProcess(process);
        }
        return bomTaskList;

    }

}

