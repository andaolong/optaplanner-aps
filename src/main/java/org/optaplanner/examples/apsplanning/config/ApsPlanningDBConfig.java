package org.optaplanner.examples.apsplanning.config;

import com.alibaba.druid.pool.DruidDataSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.sql.DataSource;

/**
 * @author andaolong
 * @time 2021/3/27-9:25
 */

@Configuration //配置类
@ComponentScan(basePackages = "org.optaplanner.examples.apsplanning") //组件扫描
public class ApsPlanningDBConfig {

    //创建数据库连接池
    @Bean
    public DruidDataSource getDruidDataSource() {
        DruidDataSource dataSource = new DruidDataSource();
        dataSource.setDriverClassName("com.mysql.jdbc.Driver");
        dataSource.setUrl("jdbc:mysql://localhost:3306/aps_andaolong");

        //这里按照自己本地的设置进行更改
        //dataSource.setUsername("root");
        //dataSource.setPassword("admin");

        //andaolong:2021年3月23日09:40:36
        dataSource.setUsername("root");
        dataSource.setPassword("260918mine");

        return dataSource;
    }

    //创建JdbcTemplate对象
    @Bean
    public JdbcTemplate getJdbcTemplate(DataSource dataSource) {
        //到ioc容器中根据类型找到dataSource
        JdbcTemplate jdbcTemplate = new JdbcTemplate();
        //注入dataSource
        jdbcTemplate.setDataSource(dataSource);
        return jdbcTemplate;
    }

}
