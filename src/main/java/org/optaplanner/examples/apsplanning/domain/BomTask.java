package org.optaplanner.examples.apsplanning.domain;

import org.optaplanner.core.api.domain.entity.PlanningEntity;
import org.optaplanner.core.api.domain.entity.PlanningPin;
import org.optaplanner.core.api.domain.variable.*;
import org.optaplanner.examples.apsplanning.domain.solver.StartTimeUpdatingVariableListener;
import org.optaplanner.examples.common.swingui.components.Labeled;

import java.util.List;

/**
 * @author andaolong
 * @time 2021/3/27-10:27
 * @describe 仿照taskassigning中的Task类
 */
//规划实体
//这里用到了TaskDifficultyComparator
//作用是：允许按难度对计划实体的集合进行排序。困难度权重估计计划某个PlanningEntity的难度。一些算法得益于首先/最后对较困难的计划实体进行计划，或者从专注于它们而受益。
//比较器应该以难度递增的方式排序（即使许多优化算法都会将其逆转）。例如：根据其RAM使用要求对3个进程进行困难排序：进程B（1GB RAM），进程A（2GB RAM），进程C（7GB RAM）
//@PlanningEntity(difficultyComparatorClass = TaskDifficultyComparator.class)
@PlanningEntity
public class BomTask extends BomTaskOrDevice implements Labeled {


    private String bomId;
    private String bomName;
    private String parentBomId;
    private BomTask parentBom;
    private String childBomId;//这个实际上没有用到
    //这两个不是从数据库中直接获取的，因为实际上一个bomTask节点可能有多个子节点，数据库没法按照这种形式进行存储，所以需要通过从数据库中根据parent_id来转化出子节点的list
    private List<String> childBomIdList;//
    private List<BomTask> childBomList;
    private String processId;//process的id
    private Process process;//task的属性，包含序号、标题、持续时间、需要的machiningType清单


    //private Priority priority;//任务的优先级

    //这个注解，表示，是否用大头针别住，比如一个被别住的规划实体在排程期间将不会改变
    //我觉得可以这样用，就是，当用户需要手动指定Task由哪个employee完成的时候，会将该task的TaskOrEmployee变量设置为该employee，然后将pinned变量设置为true，
    //这样排程的时候这个task相当于是提前pin住了，就达到了我们由用户指定某些特殊任务的目的
    @PlanningPin
    private boolean pinned;

    // Planning variables: changes during planning, between score calculations.
    //规划实体中的规划变量
    //这个规划变量的取值范围为employeeRange和taskRange，类型是TaskOrEmployee，表示当前Task的前一个步骤可能是另外一个Task也可能是一个employee
    //同时，这个规划变量是链式的，也就是运用了链模式，当然，在我们这里表示的是时间那么就可以说是链式模式在时间上的运用，也就是时间链模式
    @PlanningVariable(valueRangeProviderRefs = {"deviceRange", "bomTaskRange"}, graphType = PlanningVariableGraphType.CHAINED)
    private BomTaskOrDevice previousBomTaskOrDevice;

    // Shadow variables
    // Task nextTask inherited from superclass
    // 这是一个锚影子变量，意思是，这个employee变量是链式规划变量previousTaskOrEmployee的锚点，employee是锚
    // 源计划变量是，链接到锚的链式计划变量，也就是说，previousTaskOrEmployee会一个个的链接到employee锚变量上去
    @AnchorShadowVariable(sourceVariableName = "previousBomTaskOrDevice")
    private Device device;

    //表示这个property是一个或多个PlanningVariable的自定义阴影。开始时间是一个自定义影子变量
    //然后这里用到了两个参数
    //variableListenerClass是：源计划变量更改后，VariableListener会收到通知。然后可以根据需求进行相应的更改操作
    //sources是：触发更改此影子变量的源变量
    @CustomShadowVariable(variableListenerClass = StartTimeUpdatingVariableListener.class,
            // Arguable, to adhere to API specs (although this works), nextTask and employee should also be a source,
            // because this shadow must be triggered after nextTask and employee (but there is no need to be triggered by those)
            sources = {@PlanningVariableReference(variableName = "previousBomTaskOrDevice")})
    private Integer startTime; // In minutes

    public BomTask() {
    }

    public BomTask(long id, String bomId, String bomName, String parentBomId, String childBomId, String processId) {
        super(id);
        //long idToSetToPlanningId = Long.parseLong(bomId);
        //super(idToSetToPlanningId);
        this.bomId = bomId;
        this.bomName = bomName;
        this.parentBomId = parentBomId;
        this.childBomId = childBomId;
        this.processId = processId;
        pinned = false;
    }

    public BomTask(long id, String bomId, String bomName, String parentBomId, String childBomId, List<BomTask> childBomList, String processId) {
        super(id);
        //long idToSetToPlanningId = Long.parseLong(bomId);
        //super(idToSetToPlanningId);
        this.bomId = bomId;
        this.bomName = bomName;
        this.parentBomId = parentBomId;
        this.childBomId = childBomId;
        this.childBomList = childBomList;
        this.processId = processId;
        pinned = false;
    }


    public String getBomId() {
        return this.bomId;
    }

    public void setBomId(String bomId) {
        this.bomId = bomId;
    }

    public String getBomName() {
        return this.bomName;
    }

    public void setBomName(String bomName) {
        this.bomName = bomName;
    }

    public String getParentBomId() {
        return this.parentBomId;
    }

    public void setParentBomId(String parentBomId) {
        this.parentBomId = parentBomId;
    }

    public BomTask getParentBom() {
        return this.parentBom;
    }

    public void setParentBom(BomTask parentBom) {
        this.parentBom = parentBom;
    }

    public String getChildBomId() {
        return this.childBomId;
    }

    public void setChildBomId(String childBomId) {
        this.childBomId = childBomId;
    }

    public List<BomTask> getChildBomList() {
        return this.childBomList;
    }

    public void setChildBomList(List<BomTask> childBomList) {
        this.childBomList = childBomList;
    }

    public String getProcessId() {
        return this.processId;
    }

    public void setProcessId(String processId) {
        this.processId = processId;
    }

    public Process getProcess() {
        return process;
    }

    public void setProcess(Process process) {
        this.process = process;
    }

    public boolean isPinned() {
        return pinned;
    }

    public void setPinned(boolean pinned) {
        this.pinned = pinned;
    }

    public BomTaskOrDevice getPreviousBomTaskOrDevice() {
        return previousBomTaskOrDevice;
    }

    public void setPreviousBomTaskOrDevice(BomTaskOrDevice previousBomTaskOrDevice) {
        this.previousBomTaskOrDevice = previousBomTaskOrDevice;
    }

    @Override
    public Device getDevice() {
        return device;
    }

    public void setDevice(Device device) {
        this.device = device;
    }

    public Integer getStartTime() {
        return startTime;
    }

    public void setStartTime(Integer startTime) {
        this.startTime = startTime;
    }

    // ************************************************************************
    // Complex methods
    // ************************************************************************

    public int getMissingMachiningTypeCount() {
        if (device == null) {
            return 0;
        }
        int count = 1;

        //List<String> deviceMachiningTypeStrs = null;
        //String requiredMachiningTypeStr = process.getRequiredMachiningType().getName();
        //for (MachiningType machiningType:device.getMachiningTypes()){
        //    deviceMachiningTypeStrs.add(machiningType.getName());
        //}

        /*
        //这里用这个对象List的contain判断是否存在，不知道为什么即使是name什么的一样也不会判断包含，所以就只用里面的name进行判断是否存在了
        if (device.getMachiningTypes().contains(process.getRequiredMachiningType())) {
            //if (deviceMachiningTypeStrs.contains(requiredMachiningTypeStr)){
            count = 0;
            System.out.println("bomTask节点" + this.bomId + "---的device：" + this.device + "---具有process：" + this.process + "的MachiningType：" + process.getRequiredMachiningType());
        } else {
            System.out.println("bomTask节点" + this.bomId + "---的device：" + this.device + "---缺少process：" + this.process + "的MachiningType：" + process.getRequiredMachiningType());
            System.out.println("此device具有的machiningType有几个：" + device.getMachiningTypes().size());
            for (MachiningType machiningType : device.getMachiningTypes()) {
                System.out.println("device:" + device + "具有的machiningType有：" + machiningType);
            }
            count = 1;
        }*/


        boolean isHave = false;
        for (MachiningType machiningType : device.getMachiningTypes()) {
            if (machiningType.getName().equals(process.getRequiredMachiningType().getName())) {
                isHave = true;
            }
        }
        if (isHave) {
            count = 0;
        } else {
            count = 1;
        }


        return count;
    }

    /**
     * In minutes
     *
     * @return at least 1 minute
     */
    public int getProcessTime() {
        //Affinity affinity = getAffinity();
        return process.getProcessTime();
    }


    @Override
    public Integer getEndTime() {
        if (startTime == null) {
            return null;
        }
        return startTime + getProcessTime();
    }

    public String getCode() {
        //return taskType + "-" + indexInTaskType;
        //return process+"--"+indexInProcess;
        //这个地方返回的值会在排程的时候的调试信息中打印出来，作为当前bomTask的标记
        return "bom任务：" + this.bomId + "";
    }

    public String getTitle() {
        return process.getProcessName();
    }

    @Override
    public String getLabel() {
        return getCode() + ": " + process.getProcessName();
    }


    @Override
    public String toString() {
        return getCode();
    }

}