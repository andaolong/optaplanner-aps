package org.optaplanner.examples.apsplanning.domain;

import org.optaplanner.examples.common.domain.AbstractPersistable;
import org.optaplanner.examples.common.swingui.components.Labeled;

/**
 * @author andaolong
 * @time 2021/3/27-10:43
 * @describe 仿照taskassigning中的taskType类写的
 */
public class Process extends AbstractPersistable implements Labeled {

    private String processId;//任务的编码
    private String processName;//任务的标题
    private int processTime; // In minutes ，任务的持续时间

    private MachiningType requiredMachiningType;//任务所需要的machiningType

    public Process() {
    }

    public Process(long id, String processId, String processName, int processTime,MachiningType requiredMachiningType) {
        super(id);
        this.processId = processId;
        this.processName = processName;
        this.processTime = processTime;
        this.requiredMachiningType = requiredMachiningType;
    }

    public String getProcessId() {
        return processId;
    }

    public void setProcessId(String processId) {
        this.processId = processId;
    }

    public String getProcessName() {
        return processName;
    }

    public void setProcessName(String processName) {
        this.processName = processName;
    }

    public int getProcessTime() {
        return processTime;
    }

    public void setProcessTime(int processTime) {
        this.processTime = processTime;
    }

    public MachiningType getRequiredMachiningType() {
        return this.requiredMachiningType;
    }

    public void setRequiredMachiningType(MachiningType requiredMachiningType) {
        this.requiredMachiningType = requiredMachiningType;
    }

    // ************************************************************************
    // Complex methods
    // ************************************************************************

    @Override
    public String getLabel() {
        return "process_"+processId;
    }

    @Override
    public String toString() {
        return "process_"+processId;
    }

}
