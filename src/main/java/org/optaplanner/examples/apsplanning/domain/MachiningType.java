package org.optaplanner.examples.apsplanning.domain;

import org.optaplanner.examples.common.domain.AbstractPersistable;
import org.optaplanner.examples.common.swingui.components.Labeled;

/**
 * @author andaolong
 * @time 2021/3/27-9:57
 * @describe 仿照taskassigning中的Skill类写的
 */

public class MachiningType extends AbstractPersistable implements Labeled {

    //machiningType有一个id，有一个name
    private String machiningTypeName;

    public MachiningType() {
    }

    public MachiningType(long id, String machiningTypeName) {
        super(id);
        this.machiningTypeName = machiningTypeName;
    }

    public String getName() {
        return machiningTypeName;
    }

    public void setName(String name) {
        this.machiningTypeName = name;
    }

    // ************************************************************************
    // Complex methods
    // ************************************************************************

    @Override
    public String getLabel() {
        return machiningTypeName;
    }

    @Override
    public String toString() {
        return machiningTypeName;
    }

}