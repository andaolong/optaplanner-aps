package org.optaplanner.examples.apsplanning.dao;

import org.optaplanner.examples.apsplanning.domain.MachiningType;
import org.optaplanner.examples.apsplanning.domain.Process;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author andaolong
 * @time 2021/3/27-15:27
 * @describe
 */
@Component
public class ProcessDao {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    Long taskPlanningId = 1L;

    //查询所有的device的id，返回device类型的list，里面还未填入machiningType
    public Process getProcessByProcessIdFromDB(String processId) {

        //根据processId查询process
        String sql = "select * from process where process_id =  \"" + processId + "\"";
        //System.out.println("查询的sql是："+sql);

        //返回查询到的process
        //这里因为数据库中存储的machiningType是String类型的一个字符串，而我们的Process里面machiningType是MachiningType类型，
        //所以通过BeanPropertyRowMapper没法直接获取到，所以重写了一下这个方法，转化一下
        //Process  process = jdbcTemplate.queryForObject(sql, new BeanPropertyRowMapper<>(Process.class));
        Process process = jdbcTemplate.queryForObject(sql, new RowMapper<Process>() {
            @Override
            public Process mapRow(ResultSet resultSet, int i) throws SQLException {
                Process processTemp = new Process();
                processTemp.setProcessId(resultSet.getNString("process_id"));
                processTemp.setProcessName(resultSet.getNString("process_name"));
                processTemp.setProcessTime(resultSet.getInt("process_time"));
                String requiredMachiningTypeStr = resultSet.getNString("required_machine_type");
                MachiningType machiningTypeTemp = new MachiningType(1, requiredMachiningTypeStr);
                processTemp.setRequiredMachiningType(machiningTypeTemp);
                return processTemp;
            }
        });

        return process;
    }

    public List<Process> getAllProcess() {
        //List<Process> processList = null;
        List<Process> processList = new ArrayList<Process>();
        Process processTemp = new Process();

        //获取到processId的列表
        String sql = "select process_id from process";
        //System.out.println("查询的sql是："+sql);

        List<Process> processListOnlyHaveId = jdbcTemplate.query(sql, new BeanPropertyRowMapper<>(Process.class));

        //通过上面那个通过id获取process的方法获取process，这样就不用再写一遍machiningType的转化了
        //同时再将排程时需要的id也初始化进去
        for (Process process : processListOnlyHaveId) {
            processTemp = getProcessByProcessIdFromDB(process.getProcessId());

            String processId = processTemp.getProcessId();
            String processName = processTemp.getProcessName();
            int processTime = processTemp.getProcessTime();
            MachiningType requiredMachiningType = processTemp.getRequiredMachiningType();

            Process processNew = new Process(taskPlanningId, processId,processName,processTime,requiredMachiningType);
            taskPlanningId = taskPlanningId + 1L;
            processList.add(processNew);
        }

        return processList;
    }


}
