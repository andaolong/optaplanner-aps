/*
 * Copyright 2016 Red Hat, Inc. and/or its affiliates.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.optaplanner.examples.taskassigning.domain;

import org.optaplanner.core.api.domain.entity.PlanningEntity;
import org.optaplanner.core.api.domain.entity.PlanningPin;
import org.optaplanner.core.api.domain.variable.AnchorShadowVariable;
import org.optaplanner.core.api.domain.variable.CustomShadowVariable;
import org.optaplanner.core.api.domain.variable.PlanningVariable;
import org.optaplanner.core.api.domain.variable.PlanningVariableGraphType;
import org.optaplanner.core.api.domain.variable.PlanningVariableReference;
import org.optaplanner.examples.common.swingui.components.Labeled;
import org.optaplanner.examples.taskassigning.domain.solver.StartTimeUpdatingVariableListener;
import org.optaplanner.examples.taskassigning.domain.solver.TaskDifficultyComparator;

import com.thoughtworks.xstream.annotations.XStreamAlias;

//规划实体
//这里用到了TaskDifficultyComparator
//作用是：允许按难度对计划实体的集合进行排序。困难度权重估计计划某个PlanningEntity的难度。一些算法得益于首先/最后对较困难的计划实体进行计划，或者从专注于它们而受益。
//比较器应该以难度递增的方式排序（即使许多优化算法都会将其逆转）。例如：根据其RAM使用要求对3个进程进行困难排序：进程B（1GB RAM），进程A（2GB RAM），进程C（7GB RAM）
//PlanningEntity相关：https://docs.optaplanner.org/8.4.1.Final/optaplanner-javadoc/org/optaplanner/core/api/domain/entity/PlanningEntity.html
@PlanningEntity(difficultyComparatorClass = TaskDifficultyComparator.class)
@XStreamAlias("TaTask")
public class Task extends TaskOrEmployee implements Labeled {

    private TaskType taskType;//task的属性，包含序号、标题、持续时间、需要的skill清单
    private int indexInTaskType;
    private Customer customer;//该任务对应的顾客，也就是该任务的委托人
    private int readyTime;//该任务的准备时间
    private Priority priority;//任务的优先级

    //这个注解，表示，是否用大头针别住，比如一个被别住的规划实体在排程期间将不会改变
    //我觉得可以这样用，就是，当用户需要手动指定Task由哪个employee完成的时候，会将该task的TaskOrEmployee变量设置为该employee，然后将pinned变量设置为true，
    //这样排程的时候这个task相当于是提前pin住了，就达到了我们由用户指定某些特殊任务的目的
    @PlanningPin
    private boolean pinned;

    // Planning variables: changes during planning, between score calculations.
    //规划实体中的规划变量
    //这个规划变量的取值范围为employeeRange和taskRange，类型是TaskOrEmployee，表示当前Task的前一个步骤可能是另外一个Task也可能是一个employee
    //同时，这个规划变量是链式的，也就是运用了链模式，当然，在我们这里表示的是时间那么就可以说是链式模式在时间上的运用，也就是时间链模式
    @PlanningVariable(valueRangeProviderRefs = {"employeeRange", "taskRange"}, graphType = PlanningVariableGraphType.CHAINED)
    private TaskOrEmployee previousTaskOrEmployee;

    // Shadow variables
    // Task nextTask inherited from superclass
    // 这是一个锚影子变量，意思是，这个employee变量是链式规划变量previousTaskOrEmployee的锚点，employee是锚
    // 源计划变量是，链接到锚的链式计划变量，也就是说，previousTaskOrEmployee会一个个的链接到employee锚变量上去
    @AnchorShadowVariable(sourceVariableName = "previousTaskOrEmployee")
    private Employee employee;

    //表示这个property是一个或多个PlanningVariable的自定义阴影。开始时间是一个自定义影子变量
    //然后这里用到了两个参数
    //variableListenerClass是：源计划变量更改后，VariableListener会收到通知。然后可以根据需求进行相应的更改操作
    //sources是：触发更改此影子变量的源变量
    @CustomShadowVariable(variableListenerClass = StartTimeUpdatingVariableListener.class,
            // Arguable, to adhere to API specs (although this works), nextTask and employee should also be a source,
            // because this shadow must be triggered after nextTask and employee (but there is no need to be triggered by those)
            sources = {@PlanningVariableReference(variableName = "previousTaskOrEmployee")})
    private Integer startTime; // In minutes

    public Task() {
    }

    public Task(long id, TaskType taskType, int indexInTaskType, Customer customer, int readyTime, Priority priority) {
        super(id);
        this.taskType = taskType;
        this.indexInTaskType = indexInTaskType;
        this.customer = customer;
        this.readyTime = readyTime;
        this.priority = priority;
        pinned = false;
    }

    public TaskType getTaskType() {
        return taskType;
    }

    public void setTaskType(TaskType taskType) {
        this.taskType = taskType;
    }

    public int getIndexInTaskType() {
        return indexInTaskType;
    }

    public void setIndexInTaskType(int indexInTaskType) {
        this.indexInTaskType = indexInTaskType;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public int getReadyTime() {
        return readyTime;
    }

    public void setReadyTime(int readyTime) {
        this.readyTime = readyTime;
    }

    public Priority getPriority() {
        return priority;
    }

    public void setPriority(Priority priority) {
        this.priority = priority;
    }

    public boolean isPinned() {
        return pinned;
    }

    public void setPinned(boolean pinned) {
        this.pinned = pinned;
    }

    public TaskOrEmployee getPreviousTaskOrEmployee() {
        return previousTaskOrEmployee;
    }

    public void setPreviousTaskOrEmployee(TaskOrEmployee previousTaskOrEmployee) {
        this.previousTaskOrEmployee = previousTaskOrEmployee;
    }

    @Override
    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public Integer getStartTime() {
        return startTime;
    }

    public void setStartTime(Integer startTime) {
        this.startTime = startTime;
    }

    // ************************************************************************
    // Complex methods
    // ************************************************************************

    public int getMissingSkillCount() {
        if (employee == null) {
            return 0;
        }
        int count = 0;
        for (Skill skill : taskType.getRequiredSkillList()) {
            if (!employee.getSkillSet().contains(skill)) {
                count++;
            }
        }
        return count;
    }

    /**
     * In minutes
     *
     * @return at least 1 minute
     */
    public int getDuration() {
        Affinity affinity = getAffinity();
        return taskType.getBaseDuration() * affinity.getDurationMultiplier();
    }

    public Affinity getAffinity() {
        return (employee == null) ? Affinity.NONE : employee.getAffinity(customer);
    }

    @Override
    public Integer getEndTime() {
        if (startTime == null) {
            return null;
        }
        return startTime + getDuration();
    }

    public String getCode() {
        return taskType + "-" + indexInTaskType;
    }

    public String getTitle() {
        return taskType.getTitle();
    }

    @Override
    public String getLabel() {
        return getCode() + ": " + taskType.getTitle();
    }

    public String getToolText() {
        StringBuilder toolText = new StringBuilder();
        toolText.append("<html><center><b>").append(getLabel()).append("</b><br/>")
                .append(priority.getLabel()).append("<br/><br/>");
        toolText.append("Required skills:<br/>");
        for (Skill skill : taskType.getRequiredSkillList()) {
            toolText.append(skill.getLabel()).append("<br/>");
        }
        toolText.append("<br/>Customer:<br/>").append(customer.getName()).append("<br/>(")
                .append(getAffinity().getLabel()).append(")<br/>");
        toolText.append("</center></html>");
        return toolText.toString();
    }

    @Override
    public String toString() {
        return getCode();
    }

}
